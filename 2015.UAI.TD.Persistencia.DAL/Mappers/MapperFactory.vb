﻿Imports _2015.UAI.TD.Persistencia.Domain

Public Class MapperFactory

    Private Shared _instance As MapperFactory
    Private Shared ReadOnly _lock As Object = New Object()

    Public Shared ReadOnly Property Instance() As MapperFactory
        Get
            SyncLock _lock
                If (_instance Is Nothing) Then
                    _instance = New MapperFactory()
                End If
            End SyncLock

            Return _instance
        End Get

    End Property

    Public Function MapperFactory(Of T)() As Mapper
        'Por medio de reflexion busco el mapper que corresponde. Solo tengo que agregar el mapper al esamblado, que cumpla con la convención
        Dim name As String
        name = String.Format("_2015.UAI.TD.Persistencia.DAL.{0}Mapper", GetType(T).Name) 'Se define una convención de nombres para Los Mappers

        Try

            Dim p As Object = Reflection.Assembly.GetExecutingAssembly().CreateInstance(name)
            Return p

        Catch e As Exception

            Return Nothing
        End Try


    End Function
End Class
