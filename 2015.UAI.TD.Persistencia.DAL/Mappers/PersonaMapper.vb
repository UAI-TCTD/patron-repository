﻿Imports _2015.UAI.TD.Persistencia.Domain

Public Class PersonaMapper
    Inherits Mapper


    Public Overrides Function Map(row As DataRow) As Object

        Dim p As New Persona

        p.Id = row("PersonaId")
        p.Nombre = row("Nombre")
        p.Apellido = row("Apellido")

        Return p
    End Function
End Class
