﻿Imports _2015.UAI.TD.Persistencia.Domain

Public Class VehiculoMapper
    Inherits Mapper


    Public Overrides Function Map(row As DataRow) As Object
        Dim v As New Vehiculo
        v.Id = row("VehiculoId")
        v.Marca = row("Marca")
        v.Patente = row("Patente")

        Return v
    End Function
End Class
