﻿Imports System.Data.SqlClient

Public Interface IDbContext(Of T)
    Function GetById(id As Long) As T
    Function Save(Entity As T)
    Function GetAll() As List(Of T)
    Function GetAll(filter As String) As List(Of T)
    Function Delete(id As Long) As Boolean
End Interface
