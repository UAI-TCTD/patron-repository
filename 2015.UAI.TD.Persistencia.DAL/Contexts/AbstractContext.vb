﻿Public MustInherit Class AbstractContext(Of T)
    Implements IDbContext(Of T)


    Protected _table As String
    Protected _cmd As IDbCommand
    Protected _uow As IUnitOfWork
    Sub New()
        _uow = New AdoNetUnitOfWork()
        _table = GetType(T).Name
    End Sub

    Public Function GetById(id As Long) As T Implements IDbContext(Of T).GetById
        Dim lista As List(Of T)
        lista = GetAll(_table & "Id=" & id.ToString)
        If lista.Count = 1 Then
            Return lista(0)
        Else
            Return Nothing
        End If

    End Function
    Public MustOverride Function Save(Entity As T) As Object Implements IDbContext(Of T).Save
    Public Function GetAll() As List(Of T) Implements IDbContext(Of T).GetAll
        Return GetAll("1=1")
    End Function
    Public MustOverride Function GetAll(filter As String) As List(Of T) Implements IDbContext(Of T).GetAll
    Public MustOverride Function Delete(id As Long) As Boolean Implements IDbContext(Of T).Delete


End Class
