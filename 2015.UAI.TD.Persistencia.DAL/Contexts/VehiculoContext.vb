﻿Imports _2015.UAI.TD.Persistencia.Domain
Imports System.Data.SqlClient

Public Class VehiculoContext
    Inherits AbstractContext(Of Vehiculo)


    Public Overrides Function Delete(id As Long) As Boolean
        Return True
    End Function
    Public Overloads Overrides Function GetAll(filter As String) As List(Of Vehiculo)
        Dim cmd = _uow.CreateCommand()
        cmd.CommandText = "SELECT * FROM Vehiculos WHERE 1=1  AND " & filter
        Dim lista As New List(Of Vehiculo)
        Dim data As SqlDataAdapter = New SqlDataAdapter(cmd) '
        Dim dset As DataSet = New DataSet()
        data.Fill(dset, "Vehiculos")

        Dim v As New Vehiculo
        For Each persona As DataRow In dset.Tables("Vehiculos").Rows
            v = MapperFactory.Instance.MapperFactory(Of Vehiculo)().Map(persona)

            If Not lista.Exists(Function(c) c.Id = v.Id) Then
                lista.Add(v)
            End If

        Next
        Return lista


    End Function

   

    Public Overrides Function Save(Entity As Vehiculo) As Object
        Return Entity
    End Function
End Class
