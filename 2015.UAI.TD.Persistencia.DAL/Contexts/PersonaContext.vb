﻿Imports _2015.UAI.TD.Persistencia.Domain
Imports System.Data.SqlClient


Public Class PersonaContext
    Inherits AbstractContext(Of Persona)

    Public Overrides Function Save(Entity As Persona) As Object

        Dim sql As String

        _cmd = _uow.CreateCommand()

        _cmd.Parameters.Add(New SqlParameter("nombre", Entity.Nombre))
        _cmd.Parameters.Add(New SqlParameter("apellido", Entity.Apellido))
        If Entity.Id.Equals(0) Then

            _cmd.CommandText = "INSERT INTO Personas (Nombre,Apellido) values (@nombre, @apellido);SELECT scope_identity()"
            Entity.Id = _cmd.ExecuteScalar()

        Else

            _cmd.CommandText = "UPDATE Personas SET Nombre = @nombre, Apellido= @apellido WHERE PersonaId=@Id"
            _cmd.Parameters.Add(New SqlParameter("id", Entity.Id))

        End If


        _cmd.ExecuteNonQuery()
        _cmd.Transaction.Commit()

        Return Entity
    End Function



    Public Overrides Function GetAll(filter As String) As List(Of Persona)

        _cmd = _uow.CreateCommand()
        _cmd.CommandText = "SELECT * FROM Personas LEFT JOIN Vehiculos ON Vehiculos.Persona_Id=Personas.PersonaId WHERE 1=1  AND " & filter

        Dim lista As New List(Of Persona)
        Dim data As SqlDataAdapter = New SqlDataAdapter(_cmd) '
        Dim dset As DataSet = New DataSet()
        data.Fill(dset, "Personas")
        data.Fill(dset, "Vehiculos")

        Dim col1 As DataColumn = dset.Tables("Personas").Columns("PersonaId")
        Dim col2 As DataColumn = dset.Tables("Vehiculos").Columns("Persona_Id")
        Dim r As DataRelation = New DataRelation("pv", col1, col2, False)
        dset.Relations.Add(r)

        Dim p As New Persona

        For Each persona As DataRow In dset.Tables("Personas").Rows
            p = MapperFactory.Instance.MapperFactory(Of Persona)().Map(persona)

            For Each vehiculo As DataRow In persona.GetChildRows(r)
                p.Vehiculos.Add(MapperFactory.Instance.MapperFactory(Of Vehiculo)().Map(vehiculo))
            Next

            If Not lista.Exists(Function(c) c.Id = p.Id) Then
                lista.Add(p)
            End If

        Next
        Return lista
    End Function

    Public Overrides Function Delete(id As Long) As Boolean

        Return Nothing
    End Function
End Class
