﻿Public Interface IUnitOfWork
    Sub CommitTransaction()
    Sub RollBackTransaction()
    Function CreateCommand() As IDbCommand
    'Function CreateConnection() As IDbConnection
End Interface
