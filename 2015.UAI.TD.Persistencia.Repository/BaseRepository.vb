﻿Imports _2015.UAI.TD.Persistencia.DAL

Public Class BaseRepository(Of T)
    Implements IRepository(Of T)


    Protected _context As IDbContext(Of T)

    Sub New(context As IDbContext(Of T))
        _context = context
    End Sub


    Public Function Delete(id As Long) As Boolean Implements IRepository(Of T).Delete
        Return _context.Delete(id)
    End Function

    Public Function GetAll() As IList(Of T) Implements IRepository(Of T).GetAll
        Return _context.GetAll()
    End Function

    Public Function GetById(id As Long) As T Implements IRepository(Of T).GetById
        Return _context.GetById(id)
    End Function

    Public Function Save(entity As T) As T Implements IRepository(Of T).Save
        Return _context.Save(entity)
    End Function

    Public Function GetAll(filter As String) As IList(Of T) Implements IRepository(Of T).GetAll
        Return _context.GetAll(filter)
    End Function
End Class
