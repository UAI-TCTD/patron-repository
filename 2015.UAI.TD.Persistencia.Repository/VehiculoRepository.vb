﻿Imports _2015.UAI.TD.Persistencia.Domain
Imports _2015.UAI.TD.Persistencia.DAL

Public Class VehiculoRepository
    Inherits BaseRepository(Of Vehiculo)

    Sub New()
        MyBase.New(New VehiculoContext())
    End Sub

    Public Function GetByPersonaId(id As Long)
        Return _context.GetAll("Persona_Id=" & id.ToString)

    End Function

End Class
