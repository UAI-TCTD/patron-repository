﻿Public Interface IRepository(Of T)
    Function Save(entity As T) As T
    Function Delete(id As Long) As Boolean
    Function GetAll() As IList(Of T)
    Function GetAll(filter As String) As IList(Of T)
    Function GetById(id As Long) As T

End Interface
