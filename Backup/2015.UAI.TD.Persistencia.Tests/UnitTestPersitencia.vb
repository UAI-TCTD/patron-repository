﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports _2015.UAI.TD.Persistencia.Repository
Imports _2015.UAI.TD.Persistencia.Domain

<TestClass()> Public Class UnitTestPersitencia
    Dim personas As New PersonaRepository
    Dim vehiculos As New VehiculoRepository




    <TestMethod()> Public Sub CanGetPersonaByFilter()
        Dim l As List(Of Persona) = personas.GetAll("Nombre='roberto'")
        Assert.AreEqual(l(0).Nombre, "roberto")
    End Sub
    <TestMethod()> Public Sub CanGetPersonaById()
        Dim p As Persona = personas.GetById(1)
        Assert.AreEqual(p.Nombre, "roberto")
    End Sub
    <TestMethod()> Public Sub CanGetPersonaWithVehiculos()
        Dim p As Persona = personas.GetById(1)
        Assert.AreEqual(p.Vehiculos(0).Marca, "DS")

    End Sub
    <TestMethod()> Public Sub CanGetAllPersonas()
        Dim p As List(Of Persona) = personas.GetAll()
        Assert.AreEqual(p.Count, 9)
    End Sub


    <TestMethod()> Public Sub CanGetVehiculosFromPersona()
        Dim p As Persona = personas.GetById(1)
        Dim VehiculosDeP As List(Of Vehiculo) = vehiculos.GetByPersonaId(p.Id)
        Assert.AreEqual(VehiculosDeP.Count, 1)
    End Sub

    <TestMethod()> Public Sub CanGetVehiculoById()
        Dim v As Vehiculo = vehiculos.GetById(22)
        Assert.AreEqual(v.Marca, "DS")
    End Sub


    <TestMethod()> Public Sub CanCreatePersona()
        Dim p As New Persona
        p.Nombre = "pepe"
        p.Apellido = "Soriano"

        Assert.AreEqual(p.Id, Long.Parse(0))
        personas.Save(p)
        Assert.AreNotEqual(p.Id, Long.Parse(0))

    End Sub


    <TestMethod()> Public Sub CanUpdatePersona()


        Dim p As Persona = personas.GetById(1)
        p.Nombre = "roberto"
        personas.Save(p)

        p = personas.GetById(1)
        Assert.AreEqual(p.Nombre, "roberto")

    End Sub
End Class